import numpy as np
import scipy.io as scio
import random
import math
import matplotlib.pyplot as plt
from scipy import linalg
from sklearn.cluster import KMeans
import ipyvolume as ipv


def find_sigma(x):
    m = len(x)
    return (1 / m) * np.dot(x.T, x)


def normalize(x):
    mean = np.mean(x, axis=0)
    std = np.std(x, axis=0)
    return ((x - mean) / std), mean, std


def get_k_vectors(U, k):
    return U[:, 0: k]


def pca_revert(Z, U_red):
    return np.dot(Z, U_red.T)


def pca(X_norm, k):
    sigma = find_sigma(X_norm)
    U, S, _ = linalg.svd(sigma)
    U_red = get_k_vectors(U, k)
    return np.dot(X_norm, U_red), U_red, S


def calc_dispersion(S, K):
    return np.sum(S[0: K])/np.sum(S)


def display_images(data, count):
    m = len(data)
    n = data.shape[1]
    if count > m: count = m
    indices = random.sample(range(0, m), count)
    img_width = int(math.sqrt(n))
    img_height = img_width
    images_to_show = data[indices]
    cols = 10
    if count < 10:
        cols = count
    rows = math.ceil(count / cols)
    fig, axs = plt.subplots(nrows=rows, ncols=cols, figsize=(cols, rows))
    axs = axs.flatten()
    for i in range(len(axs)):
        ax = axs[i]
        ax.axis('off')
        if (i >= count): continue
        ax.imshow(images_to_show[i].reshape(img_width, img_height).T, cmap='gray')
    plt.show()


# 1
data1 = scio.loadmat('ex7data1.mat')
X1 = data1['X']

# 2
plt.figure(figsize=(8, 6))
plt.scatter(X1[:, 0], X1[:, 1], s=8, color='blue')
plt.xlabel('X1')
plt.ylabel('X2')
plt.grid()

# 3
norm_X1, norm_mean, norm_std = normalize(X1)
sigma1 = find_sigma(norm_X1)

print('Found sigma after normalize x1 is', sigma1.shape)

# 4
U, S, _ = linalg.svd(sigma1)
print('Coordinates', U.shape)

# 5
plt.figure(figsize=(8, 6))
plt.scatter(X1[:, 0], X1[:, 1], color='green', s=8)
u1 = U[:, 0]
u2 = U[:, 1]
plt.plot([norm_mean[0], norm_mean[0] + 2 * u1[0]], [norm_mean[1], norm_mean[1] + 2 * u1[1]], color='red', linewidth=3)
plt.plot([norm_mean[0], norm_mean[0] + u2[0]], [norm_mean[1], norm_mean[1] + u2[1]], color='blue', linewidth=3)
plt.xlabel('X1')
plt.ylabel('X2')
plt.title('Covariation matrix vectors')
plt.grid()
plt.show()

# 6
Z, U_red, _ = pca(norm_X1, 1)

plt.figure(figsize=(8, 6))
plt.plot(Z, np.zeros(len(Z)), '-')
plt.plot(Z, np.zeros(len(Z)), 'o', markersize=12, markerfacecolor='none', markeredgecolor='red')
plt.grid()
plt.show()

# 7, 8
X1_appx_norm = pca_revert(Z, U_red)
X1_appx = (X1_appx_norm * norm_std) + norm_mean

plt.figure(figsize=(8, 5))
plt.scatter(X1[:, 0], X1[:, 1], color='blue', s=8)
plt.scatter(X1_appx[:, 0], X1_appx[:, 1], color='yellow', s=8)
for i in range(len(X1)):
    plt.plot([X1[i, 0], X1_appx[i, 0]], [X1[i, 1], X1_appx[i, 1]], 'k--', markersize=1)
plt.xlabel('X1')
plt.ylabel('X2')
plt.grid()
plt.show()

# 9
data2 = scio.loadmat('ex7faces.mat')
X2 = data2['X']

# 10
display_images(X2, 100)

# 11
X2_norm, X2_norm_mean, X2_norm_std = normalize(X2)

# 12, 13
Z_36, U_red_36, S_36 = pca(X2_norm, 36)
display_images(U_red_36.T, 36)

# 14
Z_100, U_red_100, S_100 = pca(X2_norm, 100)
display_images(U_red_100.T, 50)

# 15
disp = calc_dispersion(S_100, 100) * 100
print('Dispersion % is', disp)

# 16
data3 = scio.loadmat('bird_small.mat')
A2 = data3['A'].reshape(-1, 3)
X3_norm, X3_norm_mean, X3_norm_std = normalize(A2)

# 17
clusters_size = 16
cluster = KMeans(n_clusters=clusters_size, random_state=0)
cluster.fit(X3_norm)
cmap = plt.cm.tab20
clrs = cmap(cluster.labels_)
sel = np.random.choice(len(X3_norm), 16300)
ipv.figure()

s = ipv.scatter(X3_norm[sel][:, 0], X3_norm[sel][:, 1], X3_norm[sel][:, 2], marker='sphere', size=3, color=clrs[sel])
ipv.show()
X3_norm_Z, X3_norm_U_red, X3_norm_S = pca(X3_norm, 2)
fig = plt.figure(figsize=(8, 6))
sc = plt.scatter(X3_norm_Z[:, 0], X3_norm_Z[:, 1], c=clrs)
plt.gca().invert_yaxis()
plt.show()
