import numpy as np
import pandas as pd
import scipy.io
import scipy.io as scio
from scipy import optimize
import matplotlib.pyplot as plt
from scipy.sparse.linalg import svds


def gradient_descent(initial_parameters, Y, R, num_users, num_movies, num_features, alpha, num_iters, Lambda):
    X = initial_parameters[:num_movies * num_features].reshape(num_movies, num_features)
    th = initial_parameters[num_movies * num_features:].reshape(num_users, num_features)
    J_history = []
    for i in range(num_iters):
        params = np.append(X.flatten(), th.squeeze())
        loss_res, grad = loss(params, Y, R, num_users, num_movies, num_features, Lambda)[2:]
        X_grad = grad[:num_movies * num_features].reshape(num_movies, num_features)
        th_grad = grad[num_movies * num_features:].reshape(num_users, num_features)
        X = X - (alpha * X_grad)
        th = th - (alpha * th_grad)
        J_history.append(loss_res)
    result_params = np.append(X.squeeze(), th.squeeze())
    return result_params, J_history


def loss(params, Y, R, num_users, num_movies, num_features, Lambda):
    X = params[:num_movies * num_features].reshape(num_movies, num_features)
    th = params[num_movies * num_features:].reshape(num_users, num_features)
    predictions = np.dot(X, th.T)
    err = (predictions - Y)
    J = 1 / 2 * np.sum((err ** 2) * R)
    reg_X = Lambda / 2 * np.sum(th ** 2)
    reg_Theta = Lambda / 2 * np.sum(X ** 2)
    reg_J = J + reg_X + reg_Theta

    # gradient
    X_grad = np.dot(err * R, th)
    Theta_grad = np.dot((err * R).T, X)
    grad = np.append(X_grad.squeeze(), Theta_grad.squeeze())

    # regularized gradient
    reg_X_grad = X_grad + Lambda * X
    reg_Theta_grad = Theta_grad + Lambda * th
    reg_grad = np.append(reg_X_grad.squeeze(), reg_Theta_grad.squeeze())
    return J, grad, reg_J, reg_grad


def normalize(Y, R):
    m = Y.shape[0]
    n = Y.shape[1]
    Y_mean = np.zeros((m, 1))
    Y_norm = np.zeros((m, n))
    for i in range(m):
        Y_mean[i] = np.sum(Y[i, :]) / np.count_nonzero(R[i, :])
        Y_norm[i, R[i, :] == 1] = Y[i, R[i, :] == 1] - Y_mean[i]

    return Y_norm, Y_mean


def recommend_movies(predictions, data, R, user_id, top=10):
    predictions = predictions[:, user_id]
    not_watched_R = np.where(R[:, user_id] < 1)[0]
    predictions = predictions[not_watched_R]
    recommended_idx = np.argsort(predictions)
    recommended_idx = recommended_idx[::-1]
    recommendations = []
    for i in range(top):
        idx = recommended_idx[i]
        recommendations.append(data[idx])
        print('Predictions:', data[idx])
    return recommendations


def define_personal_dataset(data):
    personal_ratings = np.zeros((1682, 1))
    personal_ratings[3] = 1
    personal_ratings[4] = 2
    personal_ratings[5] = 3
    personal_ratings[6] = 4
    personal_ratings[7] = 5

    print("Personal ratings:\n")
    for i in range(len(personal_ratings)):
        if personal_ratings[i] > 0:
            print("Rated", int(personal_ratings[i]), "for index", data[i])

    return personal_ratings

# 1
data1 = scio.loadmat('ex9_movies.mat')
Y = data1['Y']
R = data1['R']
m = Y.shape[0]
n = Y.shape[1]
X = np.zeros((1682, 10))  # num_movies X num_features
initial_theta = np.zeros((943, 10))  # num_users X num_features

# 2
num_users, num_movies, num_features = 4, 5, 3

# 3-6
X_test = X[:num_movies, :num_features]
theta_test = initial_theta[:num_users, :num_features]
Y_test = Y[:num_movies, :num_users]
R_test = R[:num_movies, :num_users]
params = np.append(X_test.squeeze(), theta_test.squeeze())

J, gradient1 = loss(params, Y_test, R_test, num_users, num_movies, num_features, 0)[:2]
print("Calculated loss for initial data:", J)
J2, gradient2 = loss(params, Y_test, R_test, num_users, num_movies, num_features, 1.5)[2:]
print("Calculated loss for initial data (lambda = 1.5):", J2)

# 7
Y_norm, Y_mean = normalize(Y, R)
num_users = Y.shape[1]
num_movies = Y.shape[0]
num_features = 10
X = np.random.randn(num_movies, num_features)
theta = np.random.randn(num_users, num_features)
initial_parameters = np.append(X.squeeze(), theta.squeeze())
params_final, J_history = gradient_descent(initial_parameters, Y, R, num_users, num_movies, num_features, 0.001, 400, 10)
plt.plot(J_history)
plt.xlabel("Epoch")
plt.ylabel("Theta")
plt.title("Loss function")
plt.show()

# 8
data2 = open('movie_ids.txt', "r").read().split("\n")[:-1]
personal_ratings = define_personal_dataset(data2)

# 9
X = params_final[:num_movies * num_features].reshape(num_movies, num_features)
Theta = params_final[num_movies * num_features:].reshape(num_users, num_features)

# Predict rating
p = np.dot(X, Theta.T)
my_predictions = p[:, 0][:, np.newaxis] + Y_mean
df = pd.DataFrame(np.hstack((my_predictions, np.array(data2)[:, np.newaxis])))
df.sort_values(by=[0], ascending=False, inplace=True)
df.reset_index(drop=True, inplace=True)
print("Top recommendations for you:\n")

for i in range(10):
    print("Predicting rating", df[1][i])

# 10
Y = np.array(Y.T)
Y = np.append(Y, [define_personal_dataset(data2).squeeze()], axis=0)
U, sigma, Vt = svds(Y - Y_mean.reshape(-1, 1).T, k=300)

sigma = np.diag(sigma)
all_user_predicted_ratings = np.dot(np.dot(U, sigma), Vt)
print("Top recommendations for you SVD:")

for i in all_user_predicted_ratings[943].argsort()[-15:][::-1]:
    print(data2[i])
