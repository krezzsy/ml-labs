from __future__ import division
import imageio
import random
import numpy as np
from scipy.cluster.hierarchy import fcluster
from scipy.cluster import hierarchy
from scipy.spatial.distance import pdist
import scipy.io as sio
import matplotlib.pyplot as plt


def init_k_rand_centers(k, n):
    centers = []
    for i in range(k):
        center = []
        for j in range(n):
            center.append(random.randint(0, 8))
        centers.append(center)
    return np.array(centers)


def init_k_rand_centers_df(X, k, min=0, max=8):
    n = X.shape[1]
    centers = np.zeros((k, n))
    for i in range(k):
        centers[i] = X[np.random.randint(min, max), :]
    return centers


def find_nearest_center(X, centers):
    K = centers.shape[0]
    idx = np.zeros((X.shape[0], 1))
    temp = np.zeros((centers.shape[0], 1))
    for i in range(X.shape[0]):
        for j in range(K):
            dist = X[i] - centers[j]
            temp[j] = np.sum(dist ** 2)
        idx[i] = np.argmin(temp) + 1
    return idx


def calc_centers(X, idx, K):
    m = X.shape[0]
    n = X.shape[1]
    counter = np.zeros((K, 1))
    centers = np.zeros((K, n))
    for i in range(m):
        index = int((idx[i] - 1)[0])
        centers[index] += X[i]
        counter[index] += 1
    return centers / counter


def get_k_means(X, idx, K, num_iters):
    for i in range(num_iters):
        centers = calc_centers(X, idx, K)
        idx = find_nearest_center(X, centers)
    return [centers, idx]


def get_k_means_with_hist(X, idx, K, num_iters):
    history = []
    for i in range(num_iters):
        [centers, idx] = get_k_means(X, idx, K, 1)
        history.append([centers, idx])
        history.append(get_k_means(X, idx, K, 1))
    return history


# 1
data1 = sio.loadmat('ex6data1.mat')
X1 = data1['X']

# 2
K = 3
initial_centers = init_k_rand_centers(K, 2)
print(initial_centers)
initial_centers = np.array([[3, 3], [6, 2], [8, 5]])

# 3
n_cent = find_nearest_center(np.array([[3.38156267, 3.38911268]]), initial_centers)
print('Nearest center is', n_cent)
centers = find_nearest_center(X1, initial_centers)

# 4
calc_centers(X1, centers, K)

# 5
get_k_means(X1, centers, K, 10)

# 6
num_iters = 10
initial_centers = init_k_rand_centers_df(X1, K)
m, n = X1.shape[0], X1.shape[1]
idx = find_nearest_center(X1, initial_centers)
fig, ax = plt.subplots(nrows=num_iters, ncols=1, figsize=(6, 36))
history = get_k_means_with_hist(X1, idx, K, num_iters)
for i in range(num_iters):
    [centers, idx] = history[i]
    color = 'rgb'
    for k in range(1, K + 1):
        grp = (idx == k).reshape(m, 1)
        ax[i].scatter(X1[grp[:, 0], 0], X1[grp[:, 0], 1], c=color[k - 1], s=15)
    ax[i].scatter(centers[:, 0], centers[:, 1], s=120, marker='x', c='black', linewidth=3)
    title = 'Epoch ' + str(i)
    ax[i].set_title(title)
plt.tight_layout()
plt.show()

# 7
data2 = sio.loadmat('bird_small.mat')
A = data2['A']
X2 = (A / 255).reshape(128 * 128, 3)

# 8
K = 16
initial_centers = init_k_rand_centers_df(X2, K, 0, 16384)
idx = find_nearest_center(X2, initial_centers)
[centers, idx] = get_k_means(X2, idx, K, num_iters)

# 9
X_recovered = X2.copy()
for i in range(1, K + 1):
    X_recovered[(idx == i).ravel(), :] = centers[i - 1]
X_recovered = X_recovered.reshape(128, 128, 3)
fig, ax = plt.subplots(1, 2)
ax[0].imshow(X2.reshape(128, 128, 3))
ax[1].imshow(X_recovered)
plt.show()

# 10
data3 = imageio.imread('bird.png')
X4 = (data3 / 255).reshape(443 * 590, 3)
K = 16
initial_centers = init_k_rand_centers_df(X4, K, 0, 16384)
idx = find_nearest_center(X4, initial_centers)
[centers, idx] = get_k_means(X4, idx, K, num_iters)
X_recovered = X4.copy()
for i in range(1, K + 1):
    X_recovered[(idx == i).ravel(), :] = centers[i - 1]
X_recovered = X_recovered.reshape(443, 590, 3)
fig, ax = plt.subplots(1, 2)
ax[0].imshow(X4.reshape(443, 590, 3))
ax[1].imshow(X_recovered)
plt.show()

# 11
data4 = imageio.imread('bird-small.png')
plt.imshow(data4)
plt.show()
data4 = data4 / 255

points = np.reshape(data4, (data4.shape[0] * data4.shape[1], data4.shape[2]))
distance_mat = pdist(points)

Z = hierarchy.linkage(distance_mat, 'single')
max_d = .3
while max_d > 0.005:
    max_d *= .5
    print(max_d)
    clusters = fcluster(Z, max_d, criterion='distance')
    meshx, meshy = np.meshgrid(np.arange(128), np.arange(128))
    plt.axis('equal')
    plt.axis('off')
    plt.scatter(meshx, -(meshy - 128), c=clusters.reshape(128, 128), cmap='inferno', marker=',')
    plt.show()
