import numpy as np
import scipy.io as scio
from mlxtend.plotting import plot_decision_regions
import matplotlib.pyplot as plt
from sklearn.svm import SVC
import nltk

def display_plot(x,y):
    x1 = x[np.where(y == 1)[0]]
    x2 = x[np.where(y == 0)[0]]
    plt.figure(figsize=(7, 6))
    plt.scatter(x1[:, 0], x1[:, 1], marker='x', c='g')
    plt.scatter(x2[:, 0], x2[:, 1], marker='o', facecolors='none', edgecolors='b')
    plt.show()

def decision_boundary_line(clf, x, y):
    fig = plt.figure(figsize=(7, 6))
    ax = plot_decision_regions(x, y.squeeze(), clf=clf, markers='ox', colors='g,b')
    ax.set_title('Decision boundary Line')
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    plt.show()

def gaussian_kernel(x1, d, sigma):
    delta = x1 - d
    return np.exp(-np.dot(delta.T, delta) / (2 * (sigma ** 2)))

def gaussian_func(X, L, sigma):
    x_m = len(X)
    l_m = len(L)
    fs = np.zeros([x_m, l_m])
    for x_i in range(x_m):
        x = X[x_i]
        for l_j in range(l_m):
            l = L[l_j]
            fs[x_i][l_j] = gaussian_kernel(x, l, sigma)
    return fs

def get_best_params(X, y, X_val, y_val, c_vals):
    best_score = 0
    best_c = 0
    best_sigma = 0

    for i in c_vals:
        for j in c_vals:
            model = SVC(C=i, kernel='rbf', gamma=1/j)
            model.fit(X, y.flatten())
            score = model.score(X_val, y_val)
            if score > best_score:
                best_score = score
                best_sigma = sigma
                best_c = i
    return (best_score, best_c, best_sigma)

def text_to_vector(text, vocab):
    n = len(vocab.keys())
    vec = np.zeros(n)
    words = pre_process_text(text).split()
    for word in words:
        if word not in vocab: continue
        idx = int(vocab[word])
        vec[idx - 1] = 1
    return vec.reshape(1, -1)

# 1
data1 = scio.loadmat('ex5data1.mat')
x_data_1 = data1['X']
y_data_1 = data1['y']

# 2
x1_1 = x_data_1[np.where(y_data_1 == 1)[0]]
x2_1 = x_data_1[np.where(y_data_1 == 0)[0]]
plt.figure(figsize=(7, 6))
plt.scatter(x1_1[:, 0], x1_1[:, 1], marker='x', c='g')
plt.scatter(x2_1[:, 0], x2_1[:, 1], marker='o', facecolors='none', edgecolors='b')
plt.show()

# 3, 4
с1 = 1
с100 = 100
clf_svm_c1 = SVC(C=с1, kernel='linear')
clf_svm_c1.fit(x_data_1, y_data_1.squeeze())

decision_boundary_line(clf_svm_c1, x_data_1, y_data_1)

clf_svm_c100 = SVC(C=с100, kernel='linear')
clf_svm_c100.fit(x_data_1, y_data_1.squeeze())
decision_boundary_line(clf_svm_c100, x_data_1, y_data_1)

# 5
x1 = np.array([3, 1, 3])
x2 = np.array([1, 4, -1])
sigma = 2
gk1 = gaussian_kernel(x1, x2, sigma)
print('Gaussian Kernel for x1=[3, 1, 3] and x2=[1, 4, -1] is', gk1)

# 6
data2 = scio.loadmat('ex5data2.mat')
x_data_2 = data2["X"]
y_data_2 = data2["y"]
x1_2 = x_data_2[np.where(y_data_2 == 1)[0]]
x2_2 = x_data_2[np.where(y_data_2 == 0)[0]]
plt.figure(figsize=(7, 6))
plt.scatter(x1_2[:, 0], x1_2[:, 1], marker='x', c='g')
plt.scatter(x2_2[:, 0], x2_2[:, 1], marker='o', facecolors='none', edgecolors='b')
plt.show()

# 7
sigma2 = 0.1
gk2 = gaussian_func(x1_2, x2_2, sigma2)

# 8, 9
gamma = np.power(sigma2, -2.) / 2
clf_svm_c1_2 = SVC(C=1, kernel='rbf', gamma=gamma)
clf_svm_c1_2.fit(x_data_2, y_data_2.squeeze())
fig = plt.figure(figsize=(7, 6))
ax = plot_decision_regions(x_data_2, y_data_2.squeeze(), clf=clf_svm_c1_2, markers='ox', colors='g,b', zoom_factor=10)
plt.show()

# 10
data3 = scio.loadmat('ex5data3.mat')
x_data_3 = data2["X"]
y_data_3 = data2["y"]
x_data_3_val = data3['Xval']
y_data_3_val = data3['yval']
display_plot(x_data_3, y_data_3)

# 11
c_vals = [0.01, 0.025, 0.1, 0.25, 0.5, 1, 2.5, 10, 25, 50, 100]
best_score, best_c, best_sigma = get_best_params(x_data_3, y_data_3.squeeze(), x_data_3_val, y_data_3_val.squeeze(), c_vals)
print("Best score is", best_score, ", best C is", best_c, ", best gamma is", best_sigma)

# 12
opt_model = SVC(C=best_c, kernel='rbf', gamma=np.power(best_sigma, -2.) / 2)
opt_model.fit(x_data_3, y_data_3.squeeze())

fig = plt.figure(figsize=(10, 6))
ax = plot_decision_regions(x_data_3, y_data_3.squeeze(), clf=opt_model, markers='ox', colors='g,b', zoom_factor=10)
plt.show()

# 13
data4 = scio.loadmat('spamTrain.mat')
x_data_4 = data2["X"]
y_data_4 = data2["y"]

# 14
clf_svm_c0_5 = SVC(C=0.1, kernel='linear')
clf_svm_c0_5.fit(x_data_4, y_data_4.squeeze())

# 15
data5 = scio.loadmat('spamTest.mat')
x_data_5 = data5["Xtest"]
y_data_5 = data5["ytest"]

# 16
c30 = 30
clf_svm_c30 = SVC(C=c30, kernel='rbf', gamma=0.001)
clf_svm_c30.fit(x_data_4, y_data_4.squeeze())

# 17
def pre_process_text(text):
    text = text.lower()
    text = nltk.re.sub('<a .*href="([^"]*)".*?<\/a>', r' \1 ', text)
    text = nltk.re.sub('<[^<>]+>', ' ', text)
    text = nltk.re.sub('(http|https)://[^\s]*', ' httpaddr ', text)
    text = nltk.re.sub('[^\s]+@[^\s]+', ' emailaddr ', text)
    text = nltk.re.sub('[0-9]+', ' number ', text)
    text = nltk.re.sub('[$]+', ' dollar ', text)
    text = nltk.re.sub('[_]+', ' ', text)
    text = nltk.re.sub('[^\w]', ' ', text)
    text = nltk.re.sub('\s+', ' ', text)
    text = text.strip()
    stemmer = nltk.stem.porter.PorterStemmer()
    words = text.split()
    stemmed_words = []
    for word in words:
        stemmed = stemmer.stem(word)
        if not len(stemmed): continue
        stemmed_words.append(stemmed)
    return ' '.join(stemmed_words)

ugly_txt = 'http://localhost:4200,331,house,#!$()<script type="text/javascript">return 0</script>keyword test 333'
processed_txt = pre_process_text(ugly_txt)
print('Text before processing: ', ugly_txt)
print('Text after processing: ', processed_txt)

# 18
vocab = open('vocab.txt', "r").read()
vocab = vocab.split("\n")
vocab_res = {}
for r in vocab:
    if r.__contains__('\t'):
        [value, key] = r.split("\t")
        vocab_res[key] = value
    else:
        continue

# 20
voc_res = text_to_vector('test email content', vocab_res)
print(np.unique(voc_res, return_counts=True))

# 20
email1 = open('emailSample1.txt', "r").read()
email2 = open('emailSample2.txt', "r").read()
spam1 = open('spamSample1.txt', "r").read()
spam2 = open('spamSample2.txt', "r").read()

# 21

lin_model = SVC(C=0.04, kernel='linear')
lin_model.fit(x_data_4, y_data_4.squeeze())
total = len(email1)
correct = 0
for i in range(len(email1)):
    item = email1[i]
    email_vec = text_to_vector(item[0], vocab_res)
    y_pred = lin_model.predict(email_vec)
    if (y_pred == item[1]): correct += 1
    print(f"File: {item[0]} - {item[1]}. Predicted: {y_pred}")
accuracy = round(correct / total, 4)
