from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeRegressor
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
from tqdm import tqdm_notebook
import matplotlib.pyplot as plt
import numpy as np


def grad(z, y):
    return y - z


def gbm_predict(X, base_algorithms_list, coefficients_list):
    return [sum([coeff * algo.predict([x])[0] for algo, coeff in zip(base_algorithms_list, coefficients_list)]) for x in
            X]


def gbm_train(X_train, y_train, max_depth=5, iters=50, coef=0.9):
    algos, coefs = [], []

    target = y_train
    for i in range(iters):
        tree = DecisionTreeRegressor(max_depth=max_depth, random_state=42)
        tree.fit(X_train, target)
        algos.append(tree)

        if isinstance(coef, (int, float)):
            coefs.append(coef)
        else:
            coefs.append(coef(i))

        target = grad(gbm_predict(X_train, algos, coefs), y_train)

    return algos, coefs


df = datasets.load_boston()
X, y = df.data, df.target
N_train = int(len(X) * 0.75)
X_train, y_train, X_test, y_test = X[:N_train], y[:N_train], X[N_train:], y[N_train:]

algos, coefs = gbm_train(X_train, y_train)
mean_squared_error(y_test, gbm_predict(X_test, algos, coefs))

algos, coefs = gbm_train(X_train, y_train, coef=lambda i: 0.9 / (1.0 + i))
mean_squared_error(y_test, gbm_predict(X_test, algos, coefs))

algos, coefs = gbm_train(X_train, y_train, coef=lambda i: 0.1, iters=100)

metrics = []

for i in range(100):
    pred = gbm_predict(X_test, algos[:i], coefs[:i])
    mse = mean_squared_error(y_test, pred)
    metrics.append(mse)

plt.plot(np.arange(100), metrics)
plt.xlabel('iter')
plt.ylabel('MSE')

metrics = []

for depth in [3, 5, 10]:
    algos, coefs = gbm_train(X_train, y_train, coef=lambda i: 0.1, max_depth=depth)
    mse = mean_squared_error(y_test, gbm_predict(X_test, algos, coefs))
    metrics.append(mse)

plt.plot([3, 5, 10], metrics)
plt.xlabel('max_depth')
plt.ylabel('MSE')

lr = LinearRegression().fit(X_train, y_train)
mean_squared_error(y_test, lr.predict(X_test))
