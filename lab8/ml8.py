import seaborn as sns
from matplotlib import pyplot as plt
from scipy.io import loadmat
import numpy as np
from scipy.stats import norm, multivariate_normal
from sklearn.metrics import f1_score

data = loadmat('ex8data1.mat')
X, X_val, y_val = data['X'], data['Xval'], data['yval'].ravel()
X.shape
sns.scatterplot(X[:, 0], X[:, 1])
plt.show()
std1 = np.std(X[:, 0])
mean1 = np.mean(X[:, 0])
print(mean1, std1)
plt.plot(norm.pdf(np.linspace(mean1-3*std1, mean1+3*std1, 100), mean1, std1))
plt.show()
std2 = np.std(X[:, 1])
mean2 = np.mean(X[:, 1])
print(mean2, std2)
plt.plot(norm.pdf(np.linspace(mean2-3*std2, mean2+3*std2, 100), mean2, std2))
plt.show()
x, y = np.mgrid[np.min(X[:, 0])-1:np.max(X[:, 0])+1:.1, np.min(X[:, 1])-1:np.max(X[:, 1])+1:.1]
pos = np.dstack((x, y))
rv = multivariate_normal([mean1, mean2], [[std1, 0.], [0., std2]])
plt.figure(figsize=(8, 8))
plt.contourf(x, y, rv.pdf(pos), levels=np.linspace(0, rv.pdf([mean1, mean2]), 50))
plt.scatter(X[:, 0], X[:, 1], color='b')
plt.show()
max_pdf = rv.pdf([mean1, mean2])
pdf_threshold = np.linspace(0, max_pdf, 1000)

f1_scores = []
for thr in pdf_threshold:
    y_pred = (rv.pdf(X_val) < thr).astype(int)
    f1_scores.append(f1_score(y_pred=y_pred, y_true=y_val))

plt.plot(pdf_threshold, f1_scores)

f1_scores = np.array(f1_scores)
potential_thr = pdf_threshold[f1_scores==f1_scores.max()]
potential_thr

x, y = np.mgrid[np.min(X[:, 0])-1:np.max(X[:, 0])+1:.1, np.min(X[:, 1])-1:np.max(X[:, 1])+1:.1]
pos = np.dstack((x, y))
rv = multivariate_normal([mean1, mean2], [[std1**2, 0.], [0., std2**2]])
plt.figure(figsize=(8, 8))
plt.contourf(x, y, rv.pdf(pos), levels=np.linspace(0, rv.pdf([mean1, mean2]), 50))

mask_anom = rv.pdf(X) < np.mean(potential_thr)
plt.scatter(X[mask_anom][:, 0], X[mask_anom][:, 1], color='r')
plt.scatter(X[~mask_anom][:, 0], X[~mask_anom][:, 1], color='b')

data = loadmat('ex8data2.mat')
X, X_val, y_val = data['X'], data['Xval'], data['yval'].ravel()
mean_vec = np.mean(X, axis=0)
cov_mtx = np.cov(X.T)
rv = multivariate_normal(mean_vec, cov_mtx)

max_pdf = rv.pdf(mean_vec)
pdf_threshold = np.linspace(0, max_pdf, 1000)

f1_scores = []
for thr in pdf_threshold:
    y_pred = (rv.pdf(X_val) < thr).astype(int)
    f1_scores.append(f1_score(y_pred=y_pred, y_true=y_val))

plt.plot(pdf_threshold, f1_scores)
f1_scores = np.array(f1_scores)
potential_thr = pdf_threshold[f1_scores==f1_scores.max()]
potential_thr

y_pred = rv.pdf(X) < np.mean(potential_thr)
y_pred.sum()

