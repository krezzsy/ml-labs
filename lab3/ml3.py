import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from mpl_toolkits.mplot3d import Axes3D
from scipy.optimize import minimize
from scipy import optimize
import scipy.io as sio


def h0x(x, theta):
    return np.dot(x, theta)


def loss_func_reg(theta, x, y, lambda_):
    m = len(y)
    predictions = h0x(x, theta)
    sqe = np.sum(np.square(predictions - y))
    reg = np.sum(lambda_ * np.square(theta[1:]))
    j = 1 / (2 * m) * (sqe + reg)
    return j


def gradient_reg(theta, x, y, lambda_):
    predictions = h0x(x, theta)
    grad = np.dot(x.transpose(), (predictions - y))
    reg = lambda_ * theta
    reg[0] = 0
    return (grad + reg) / len(y)


def build_curves_plot(x_train, y_train, x_val, y_val, lambda_):
    m = len(y_train)
    train_err = np.zeros(m)
    validation_err = np.zeros(m)
    for i in range(1, m):
        theta = optimize.fmin_cg(loss_func_reg, np.zeros(x_train.shape[1]), gradient_reg, (x_train[0:i + 1, :], y_train[0:i + 1], lambda_))
        train_err[i] = loss_func_reg(theta, x_train[0:i + 1, :], y_train[0:i + 1], lambda_)
        validation_err[i] = loss_func_reg(theta, x_val, y_val, lambda_)
    plt.plot(range(2, m + 1), train_err[1:])
    plt.plot(range(2, m + 1), validation_err[1:])
    plt.xlabel("Training examples numbers")
    plt.ylabel("Errors")
    plt.legend(["Training", "Validation"])
    plt.grid()
    plt.show()


def polynom(x1, pol_power):
    polynom_res = np.zeros(shape=(len(x1), pol_power))
    for i in range(0, pol_power):
        polynom_res[:, i] = x1.squeeze() ** (i + 1)
    return polynom_res


# 1
data1 = sio.loadmat('ex3data1.mat')
x_train = data1['X']
X_train = np.hstack((np.ones((len(x_train), 1)), x_train))
theta = np.zeros(X_train.shape[1])
x_val = data1['Xval']
x_test = data1['Xtest']
y_train = data1['y']
y_val = data1['yval']
y_test = data1['ytest']

# 2
fig, axis = plt.subplots()
axis.scatter(x_train, y_train)
plt.xlabel('Water level change')
plt.ylabel('Water level flowing out of the dam')
# plt.show()

# 3
lambda_0 = 0.0
loss_1 = loss_func_reg(theta, X_train, y_train, lambda_0)
print("Loss function result is", loss_1)

# 4
grad_1 = gradient_reg(theta, X_train, y_train.squeeze(), lambda_0)
print("Gradient function result is:", grad_1)

# 5
nlm_opt_theta = optimize.fmin_cg(f=loss_func_reg, x0=theta, args=(X_train, y_train.squeeze(), lambda_0))

h = np.dot(X_train, nlm_opt_theta)
axis.plot(X_train[:, 1], h, color='red')
plt.show()

# 6
plt.title("Learning Curves Plot")
build_curves_plot(X_train, y_train.squeeze(), np.hstack((np.ones((len(x_val), 1)), x_val)), y_val.squeeze(), lambda_0)


# 7
pol_power = 8
x_train_pol_res = polynom(x_train, pol_power)
x_val_pol_res = polynom(x_val, pol_power)
x_test_pol_res = polynom(x_test, pol_power)

# 8
def normalize(df):
    result = df.copy()
    for feature_name in df.columns:
        mean_value = df[feature_name].mean()
        std_value = df[feature_name].std()
        result[feature_name] = (df[feature_name] - mean_value) / std_value
    return result


train_means = x_train_pol_res.mean(axis=0)
train_std = x_train_pol_res.std(axis=0, ddof=1)

x_train_pol_norm = (x_train_pol_res - train_means) / train_std
x_val_pol_norm = (x_val_pol_res - train_means) / train_std
x_test_pol_norm = (x_test_pol_res - train_means) / train_std

x_train_pol_norm = np.hstack((np.ones((len(x_train_pol_norm), 1)), x_train_pol_norm))
x_val_pol_norm = np.hstack((np.ones((len(x_val_pol_norm), 1)), x_val_pol_norm))
x_test_pol_norm = np.hstack((np.ones((len(x_test_pol_norm), 1)), x_test_pol_norm))

# 9
cg_opt_theta1 = optimize.fmin_cg(loss_func_reg, np.zeros(x_train_pol_norm.shape[1]), gradient_reg, (x_train_pol_norm, y_train.squeeze(), lambda_0))
x_tr_line1 = np.linspace(min(x_train) - 5, max(x_train) + 5, 1000)
x_tr_line1_polynom = polynom(x_tr_line1, 8)
x_tr_line1_polynom = normalize(pd.DataFrame(x_tr_line1_polynom))
x_tr_line1_polynom = np.hstack((np.ones((len(x_tr_line1_polynom), 1)), x_tr_line1_polynom))

# 10
plt.scatter(x_train, y_train, color='red')
plt.plot(x_tr_line1, h0x(x_tr_line1_polynom, cg_opt_theta1))
plt.title("Polynomial Fit (lambda=0)")
plt.xlabel('Water level change')
plt.ylabel('Water level flowing out of the dam')
plt.show()

build_curves_plot(x_train_pol_norm, y_train.squeeze(), x_val_pol_norm, y_val.squeeze(), lambda_0)

# 11
lambda_1 = 1
cg_opt_theta2 = optimize.fmin_cg(loss_func_reg, np.zeros(x_train_pol_norm.shape[1]), gradient_reg, (x_train_pol_norm, y_train.squeeze(), lambda_1))
x_tr_line2 = np.linspace(min(x_train) - 5, max(x_train) + 5, 1000)
x_tr_line2_polynom = polynom(x_tr_line2, 8)
x_tr_line2_polynom = normalize(pd.DataFrame(x_tr_line2_polynom))
x_tr_line2_polynom = np.hstack((np.ones((len(x_tr_line2_polynom), 1)), x_tr_line2_polynom))

plt.scatter(x_train, y_train, color='red')
plt.plot(x_tr_line2, h0x(x_tr_line2_polynom, cg_opt_theta2))
plt.title("Polynomial Fit (lambda=1)")
plt.xlabel('Water level change')
plt.ylabel('Water level flowing out of the dam')
plt.show()

lambda_2 = 100
cg_opt_theta3 = optimize.fmin_cg(loss_func_reg, np.zeros(x_train_pol_norm.shape[1]), gradient_reg, (x_train_pol_norm, y_train.squeeze(), lambda_2))
x_tr_line3 = np.linspace(min(x_train) - 5, max(x_train) + 5, 1000)
x_tr_line3_polynom = polynom(x_tr_line3, 8)
x_tr_line3_polynom = normalize(pd.DataFrame(x_tr_line3_polynom))
x_tr_line3_polynom = np.hstack((np.ones((len(x_tr_line3_polynom), 1)), x_tr_line3_polynom))

plt.scatter(x_train, y_train, color='red')
plt.plot(x_tr_line3, h0x(x_tr_line3_polynom, cg_opt_theta3))
plt.title("Polynomial Fit (lambda=100)")
plt.xlabel('Water level change')
plt.ylabel('Water level flowing out of the dam')
plt.show()

# 12
lambdas = [0, 0.01, 0.1, 1, 10]
validation_errs = []
for l in lambdas:
    opt_th = optimize.fmin_cg(loss_func_reg, np.zeros(x_train_pol_norm.shape[1]), gradient_reg, (x_train_pol_norm, y_train.squeeze(), l))
    validation_errs.append(loss_func_reg(opt_th, x_val_pol_norm, y_val.squeeze(), 0.0))
plt.plot(lambdas, validation_errs)
plt.axis([0, len(lambdas), 0, validation_errs[-1] + 1])
plt.grid()
plt.xlabel("Lambda")
plt.ylabel("Error")
plt.title("Validation Curve")
plt.show()
