import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import time

# yˆ = hθ(x) = θ0 + θ1
def h0x_func(x, theta):
    return theta[0] + theta[1] * x

def h0x_func_vectorized(x, theta):
    return x.dot(theta)

# J(θ0, θ1) = (1/2m)*(Sum(yˆ[i]-y[i]), i=1..m)
def get_loss(x, y, m, theta):
    diff = []
    for i in range(0, m):
        val = pow(h0x_func(x[i], theta) - y[i], 2)
        diff.append(val)
    j = (1 / (2 * m)) * sum(diff)
    return j

def get_loss_vectorized(x, y, m, theta):
    temp = (h0x_func_vectorized(x, theta) - y)
    return (1 / (2 * m)) * np.dot(temp.T, temp)[0][0]

def get_gradient_descent(x, y, theta, m, r, iters):
    for i in range(iters):
        val = np.zeros(len(theta))
        for j in range(0, m):
            if isinstance(x[j], float):
                val[0] += h0x_func(x[j], theta) - y[j]
                for n in range(1, len(theta)):
                    val[n] += (h0x_func(x[j], theta) - y[j]) * x[j]
            if isinstance(x[j], np.ndarray):  # in case X is vector
                for n in range(1, len(theta)):
                    val[n] += (h0x_func_vectorized(x[j], theta) - y[j]) * x[j][n]
        for t in range(0, len(theta)):
            theta[t] = theta[t] - (r / m) * val[t]
    return theta

def get_gradient_descent_vectorized(x, y, theta, m, r, iters):
    for i in range(iters):
        h0x = (h0x_func_vectorized(x, theta) - y).T
        dt = np.dot(h0x, x).T
        learning_rate = r * (1 / m) * dt
        theta = theta - learning_rate
    return theta

def loss_func_arr(X, y, x1, x2):
    f = np.zeros((len(x1), len(x2)))
    for i in range(0, len(x1)):
        for j in range(0, len(x2)):
            f[i][j] = get_loss(X, y, len(X), [x1[i], x2[j]])
    return f

def draw_loss_plot(x1, x2, f):
    fig = plt.figure()
    ax = fig.gca(projection='3d')

    x1, x2 = np.meshgrid(x1, x2)
    ax.plot_surface(x1, x2, f, linewidth=0, antialiased=False)
    plt.show()

def draw_loss_contour_plot(x1, x2, f):
    fig, ax = plt.subplots()
    x1, x2 = np.meshgrid(x1, x2)

    ax.contour(x1, x2, f, levels=[20, 50, 125, 300, 750, 1800, 4500, 11250, 28000])
    plt.show()

def normalize(df):
    result = df.copy()
    for feature_name in df.columns:
        max_value = df[feature_name].max()
        min_value = df[feature_name].min()
        result[feature_name] = (df[feature_name] - min_value) / (max_value - min_value)
    return result

# 1
data1 = pd.read_csv('lab1\ex1data1.txt', header=None)
x = data1[0]  # population, x
y = data1[1]  # restaurant profit, target

# 2
fig, ax = plt.subplots()
ax.scatter(x, y)
# plt.show()
corr = np.corrcoef(x, y)[0, 1]
print('Correlation between population and restaurant profit is:', corr)

# 3
theta = [0, 0]
m = len(x)
loss_val = get_loss(x, y, m, theta)
print('Function loss result is:', loss_val)

# 4
learn_rate = 0.0001
iters = 1000
new_theta = get_gradient_descent(x.values, y.values, theta, m, learn_rate, iters)
print('Theta result of gradient descent is :', new_theta)
plot_range = [0, 25]
plot_start = h0x_func(0, new_theta)
plot_end = h0x_func(25, new_theta)
ax.plot(plot_range, [plot_start, plot_end], 'red')
plt.show()

# 5
x1 = x2 = np.arange(-15, 15, 0.25)
f = loss_func_arr(x, y, x1, x2)
draw_loss_plot(x1, x2, f)
draw_loss_contour_plot(x1, x2, f)

# 6
data2 = pd.read_csv('lab1\ex1data2.txt', header=None)
x2 = data2.iloc[:, 0:2]
y2 = data2.iloc[:, 2]

# 7
learn_rate = 0.0001
iters = 1000
theta = np.zeros((3, 1))
m = len(x2)
x2_norm = normalize(x2)
x2_norm.insert(0, '1s', 1)

# 8
start_vector = time.time()
loss_vect = get_loss_vectorized(x2_norm.values, y2.values, m, theta)
theta = get_gradient_descent_vectorized(x2_norm.values, y2.values, theta, m, learn_rate, iters)
time_vector = time.time() - start_vector
print('Vectorized gradient descent execution time is:', time_vector)

# 9
start_not_vector = time.time()
theta = get_gradient_descent(x2_norm.values, y2, [0, 0, 0], m, learn_rate, iters)
time_not_vector = time.time() - start_not_vector
print('Non-vectorized gradient descent execution time is:', time_not_vector)
print('Time boost between vectorized and non-vectorized is:', time_not_vector-time_vector)

# 10
learn_rate = 0.01 # 100x greater
inc_theta = get_gradient_descent_vectorized(x2_norm.values, y2.values, theta, m, learn_rate, iters)
inc_loss = get_loss(x2_norm.values, y2.values, m, inc_theta)
