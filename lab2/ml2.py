import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from mpl_toolkits.mplot3d import Axes3D
from scipy.optimize import minimize
from scipy import optimize
import scipy.io as sio

def sigmoid(z):
    return 1 / (1 + np.exp(-z))


def loss_func(theta, x, y, lambda_=0.0):
    m = len(y)
    predictions = sigmoid(np.dot(x, theta))
    if lambda_ == 0:
        j = (1 / m) * ((np.dot(-y.T, np.log(predictions))) - np.dot((1 - y).T, np.log(1 - predictions)))
    else:  # L2-regularized if lambda_ presents
        j = (1 / m) * ((np.dot(-y.T, np.log(predictions))) - np.dot((1 - y).T, np.log(1 - predictions))) + (
                lambda_ / (2 * m)) * np.sum(theta[1:] ** 2)
    return j


def gradient(theta, x, y):
    m = len(y)
    predictions = sigmoid(np.dot(x, theta))
    return 1 / m * np.dot(x.transpose(), (predictions - y))

def gradient_reg(theta, x, y, lambda_):
    m = len(y)
    grad = gradient(theta, x, y)
    grad[1:] = grad[1:] + (lambda_ / m) * theta[1:]
    return grad

def predict_exam(ex1_sc, ex2_sc, theta):
    x_test = np.array([ex1_sc, ex2_sc])
    x_test = np.append(np.ones(1), x_test)
    return sigmoid(x_test.dot(theta))

def predict_test(x, theta):
    x_test = np.asarray(x[0])
    x_test = np.append(np.ones(1), x_test)
    return sigmoid(x_test.dot(theta))

def polynom(x1, x2, pol_power):
    polynom_res = []
    for i in range(0, pol_power + 1):
        for j in range(0, pol_power + 1):
            if i + j <= pol_power:
                polynom_res.append((x1**i)*(x2**j))
    return polynom_res

def extend_x_with_polynom(x):
    x_ext = []
    for i in range(0, len(x2.values)):
        x_ext.append(polynom(x2.values[i][0], x2.values[i][1], 6))
    return x_ext

def get_plot_dots(x, y, theta):
    f = np.zeros((len(xd), len(yd)))
    for i in range(len(x)):
        for j in range(len(yd)):
            dots = [1]
            dots.extend(polynom(x[i], y[j], 6))
            f[i, j] = sigmoid(np.dot(np.array(dots).T, theta))
    return f

def predict_num(x, theta):
    return np.argmax(np.asarray(np.dot(x, theta.T)))

# 1
data1 = pd.read_csv('ex2data1.txt', header=None)
x1 = data1.iloc[:, 0:2]
y1 = data1.iloc[:, 2]

# 2
x1_succ = data1.loc[y1.isin([1])].iloc[:, 0:2]
x1_fail = data1.loc[y1.isin([0])].iloc[:, 0:2]
plt.xlabel('First exam result')
plt.ylabel('Second exam result')
passed_plt = plt.scatter(x1_succ[0], x1_succ[1], color='Green')
failed_plt = plt.scatter(x1_fail[0], x1_fail[1], color='Red')
plt.legend((passed_plt, failed_plt), ('Passed', 'Failed'))
# plt.show()

# 3
m = x1.shape[0]
n = x1.shape[1]
x1 = np.append(np.ones((m, 1)), x1, axis=1)
y1 = y1.values.reshape(m, 1)
theta = np.zeros((n+1, 1))
loss = loss_func(theta, x1, y1)
grad = gradient(theta, x1, y1)
print("Loss function result is", loss)
print("Gradient function result is:", grad)

# 4
nlm_opt_theta = optimize.minimize(fun=loss_func, x0=theta, args=(x1, y1), method='Nelder-Mead')
nlp_opt_loss = loss_func(nlm_opt_theta.x, x1, y1)
bfgs_opt_theta = optimize.fmin_bfgs(f=loss_func, x0=theta, fprime=gradient, args=(x1, y1.T[0]))
bfgs_opt_loss = loss_func(bfgs_opt_theta, x1, y1)

# 5
prob = predict_exam(53, 78, bfgs_opt_theta)
print("Result prediction for scores 53 and 78 probability is:", prob)

# 6
plot_x = [np.min(x1[:, 1] - 2), np.max(x1[:, 2] + 2)]
plot_y = -1 / bfgs_opt_theta[2] * (bfgs_opt_theta[0] + np.dot(bfgs_opt_theta[1], plot_x))
# Plotting the Single Line Decision Boundary
dcs_bnd_plt, = plt.plot(plot_x, plot_y, label='Decision Boundary', color='Blue')
plt.legend((passed_plt, failed_plt, dcs_bnd_plt), ('Passed', 'Failed', 'Decision Boundary'))
plt.show()

# 7
data2 = pd.read_csv('ex2data2.txt', header=None)
x2 = data2.iloc[:, 0:2]
y2 = data2.iloc[:, 2]

# 8
x2_succ = data2.loc[y2.isin([1])].iloc[:, 0:2]
x2_fail = data2.loc[y2.isin([0])].iloc[:, 0:2]
plt.xlabel('First test results')
plt.ylabel('Second test results')
test_passed_plt = plt.scatter(x2_succ[0], x2_succ[1], color='Green')
test_failed_plt = plt.scatter(x2_fail[0], x2_fail[1], color='Red')
plt.legend((test_passed_plt, test_failed_plt), ('Passed', 'Failed'))
plt.show()

# 9
polynom_results = polynom(x2[0], x2[1], 6)

# 10
x_ext = extend_x_with_polynom(x2)
x_ext_df = pd.DataFrame(x_ext)
x_ext_df.insert(0, '1s', 1)

m = x_ext_df.shape[0]
n = x_ext_df.shape[1]
theta = np.zeros((n, 1))

lambda_ = 0.1
theta_tnc, nf, rc = optimize.fmin_tnc(func=loss_func, x0=theta, fprime=gradient_reg, args=(x_ext_df, y2,  lambda_))
print(theta_tnc)
theta_tnc_opt_loss2 = loss_func(theta_tnc, x_ext_df, y2)
print("Optimized theta for TNC:", theta_tnc_opt_loss2)

# 11
theta_bfgs = optimize.fmin_bfgs(f=loss_func, x0=theta, fprime=gradient_reg, args=(x_ext_df, y2, lambda_))
bfgs_opt_loss2 = loss_func(theta_bfgs, x_ext_df, y2)
print("Optimized theta for BFGS:", bfgs_opt_loss2)
theta_nlm = optimize.minimize(fun=loss_func, x0=theta, args=(x_ext_df, y2), method='Nelder-Mead')
nlm_opt_loss2 = loss_func(theta_nlm.x, x_ext_df, y2)
print("Optimized theta for Nelder-Mead:", nlm_opt_loss2)

# 12
prob2 = predict_test(x_ext, theta_bfgs)
print('Result prediction for X[0] probability is: ', prob2)

# 13,14
lambda2_ = 0.01
theta_bfgs2 = optimize.fmin_bfgs(loss_func, theta, gradient_reg, (x_ext_df, y2, lambda2_))
lambda3_ = 0.001
theta_bfgs3 = optimize.fmin_bfgs(loss_func, theta, gradient_reg, (x_ext_df, y2, lambda3_))

xd = yd = np.linspace(-1, 1, 50)
f = np.zeros((len(xd), len(yd)))
f1 = get_plot_dots(xd, yd, theta_bfgs)
f2 = get_plot_dots(xd, yd, theta_bfgs2)
f3 = get_plot_dots(xd, yd, theta_bfgs3)

X = data2.iloc[:, :-1]
plt.scatter(x2_succ[0], x2_succ[1], color='Green')
plt.scatter(x2_fail[0], x2_fail[1], color='Red')
plt.contour(xd, yd, f1, 0, colors='Orange')
plt.contour(xd, yd, f2, 0, colors='Black')
plt.contour(xd, yd, f3, 0, colors='Purple')
orange_patch = mpatches.Patch(color='orange', label='lambda = ' + str(lambda_))
black_patch = mpatches.Patch(color='black', label='lambda = ' + str(lambda2_))
purple_patch = mpatches.Patch(color='purple', label='lambda = ' + str(lambda3_))
plt.legend(handles=[orange_patch, black_patch, purple_patch])
plt.xlabel('First test results')
plt.ylabel('Second test results')
plt.show()

# 15
data3 = sio.loadmat('ex2data3.mat')
x3 = data3['X']
y3 = data3['y']

def load_images_keys(y3, data_images):
    for i in range(len(y3)):
        data_images[y3[i][0]] = i
    keys = data_images.keys()
    return keys

# 16
data_images = {}
data_images_keys = load_images_keys(y3, data_images)
fig, axis = plt.subplots(1, 10)

for i in range(len(data_images_keys)):
    axis[i].imshow(x3[data_images.get(list(data_images_keys)[i]), :].reshape(20, 20, order="F"), cmap="hot")
    axis[i].axis("off")

plt.show()

# 17
x3_df = pd.DataFrame(x3)
x3_df.insert(0, '1s', 1)
(m, n) = x3_df.shape
lambda_ = 0.1
theta = np.zeros((n, 1))
loss3_1 = loss_func(theta, x3_df, y3)
grad3_1 = gradient(theta, x3_df, y3)
print("Loss function result is", loss3_1)
print("Gradient function result is:", grad3_1)

# 18
loss3_2 = loss_func(theta, x3_df, y3, lambda_)
grad3_2 = gradient_reg(theta, x3_df, y3, lambda_)
print("Regularized Loss function result is", loss3_2)
print("Regularized Gradient function result is:", grad3_2)

# 19
theta = np.zeros((10, n))
theta_3 = []
for i in range(10):
    digit_class = i if i else 10
    theta[i] = optimize.fmin_bfgs(f=loss_func, x0=theta[i], fprime=gradient_reg, args=(x3_df, (y3 == digit_class).flatten().astype(np.int), lambda_), maxiter=50)
    theta_3.append(10 * theta[i])

# 20
print("Predicted number:", predict_num(x3_df.values[1234], np.asarray(theta_3)), "Real:", y3[1234][0])

# 21
pred = np.argmax(np.dot(x3_df.values, np.asarray(theta_3).T), axis=1)
pred_conv = [e if e else 10 for e in pred]  # convert 0 to 10
pred_succ_count = 0
for i in range(len(pred_conv)):
    if pred_conv[i] == y3[i][0]:
        pred_succ_count += 1

pred_acc = (pred_succ_count / len(y3)) * 100
print("Predictors's accuracy: ", pred_acc)
