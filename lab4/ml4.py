from __future__ import division
import scipy.io as sio
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy import optimize as opt


def sigmoid(z):
    return 1 / (1 + np.exp(-z))


def sigmoid_der(z):
    return np.multiply(sigmoid(z), 1 - sigmoid(z))


def get_random_weights(l_in, l_out):
    w = np.random.rand(l_out, l_in + 1) * (2 * 0.1) - 0.1
    return w


def h0x_func(x, theta):
    m = len(x)
    z = sigmoid(np.dot(np.hstack((np.ones((m, 1)), x)), theta[0].T))
    z = np.hstack((np.ones((m, 1)), z))
    return sigmoid(np.dot(z, theta[1].T))


def loss_func(neunet_params, hidden_layer_size, labels_size, X, y_d, lambda_):
    m = len(X)
    input_layer_size = len(X[0])
    theta1 = np.reshape(neunet_params[:hidden_layer_size * (input_layer_size + 1)], (hidden_layer_size, input_layer_size + 1), 'F')
    theta2 = np.reshape(neunet_params[hidden_layer_size * (input_layer_size + 1):], (labels_size, hidden_layer_size + 1), 'F')
    h = h0x_func(X, [theta1, theta2])
    temp = np.sum(np.multiply(y_d, np.log(h)) + np.multiply(1 - y_d, np.log(1 - h)))
    sum1 = np.sum(np.sum(np.power(theta1[:, 1:], 2), axis=1))
    sum2 = np.sum(np.sum(np.power(theta2[:, 1:], 2), axis=1))
    return np.sum(temp / (-m)) + (sum1 + sum2) * lambda_ / (2 * m)


def display_figures(x):
    if x.ndim == 2:
        m = x.shape[0]
        n = x.shape[1]
    elif x.ndim == 1:
        n = X.size
        m = 1
        x = x[None]
    w = int(np.round(np.sqrt(n)))
    rows = int(np.floor(np.sqrt(m)))
    cols = int(np.ceil(m / rows))
    fig, ax_array = plt.subplots(rows, cols, figsize=(10, 10))
    fig.subplots_adjust(wspace=0.025, hspace=0.025)
    ax_array = [ax_array] if m == 1 else ax_array.ravel()
    for i, ax in enumerate(ax_array):
        ax.imshow(X[i].reshape(w, w, order='F'), extent=[0, 1, 0, 1])
        ax.axis('off')  # hide labels
    plt.show()


def back_prop(neunet_params, hidden_layer_size, labels_size, X, y_d, lambda_):
    m = len(y_d)
    input_layer_size = len(X[0])
    initial_theta1 = np.reshape(neunet_params[:hidden_layer_size * (input_layer_size + 1)],
                                (hidden_layer_size, input_layer_size + 1), 'F')
    initial_theta2 = np.reshape(neunet_params[hidden_layer_size * (input_layer_size + 1):],
                                (labels_size, hidden_layer_size + 1), 'F')
    delta1 = np.zeros(initial_theta1.shape)
    delta2 = np.zeros(initial_theta2.shape)

    for i in range(X.shape[0]):
        a1 = np.hstack((np.ones(1), X[i]))
        z2 = np.dot(a1, initial_theta1.T)
        a2 = np.hstack((np.ones(1), sigmoid(z2)))
        z3 = np.dot(a2, initial_theta2.T)
        a3 = sigmoid(z3)
        d3 = a3 - y_d.iloc[i, :][np.newaxis, :]
        z2 = np.hstack((np.ones(1), z2))
        d2 = np.multiply(np.dot(initial_theta2.T, d3.T), sigmoid_der(z2).T[:, np.newaxis])
        delta1 = delta1 + np.dot(d2[1:, :], a1[np.newaxis, :])
        delta2 = delta2 + np.dot(d3.T, a2[np.newaxis, :])
    delta1 /= m
    delta2 /= m
    delta1[:, 1:] = delta1[:, 1:] + initial_theta1[:, 1:] * lambda_ / m
    delta2[:, 1:] = delta2[:, 1:] + initial_theta2[:, 1:] * lambda_ / m
    return np.hstack((delta1.ravel(order='F'), delta2.ravel(order='F')))


def gradient_check(nn_initial_params, nn_backprop_params, hidden_layer_size, X, y_d, lambda_, e_check):
    n_elems = len(nn_initial_params)
    for i in range(5):
        rand = int(np.random.rand() * n_elems)
        eps = np.zeros((n_elems, 1))
        eps[rand] = e_check
        loss_high = loss_func(nn_initial_params + eps.squeeze(), hidden_layer_size, len(y_one_hot_trans.values[0]), X, y_d, lambda_)
        loss_low = loss_func(nn_initial_params - eps.squeeze(), hidden_layer_size, len(y_one_hot_trans.values[0]),  X, y_d, lambda_)
        g = (loss_high - loss_low) / float(2 * e_check)
        print("Gradient/Back Propagation Gradient for element[" + str(i) + "] equals", (g, nn_backprop_params[rand]))

# 1
data = sio.loadmat('ex4data1.mat')
X = data.get('X')
y = data.get('y')

# 2
weights = sio.loadmat('ex4weights.mat')
theta1 = weights.get('Theta1')
theta2 = weights.get('Theta2')
neunet_params = np.hstack((theta1.ravel(order='F'), theta2.ravel(order='F')))
hidden_layer_size = len(theta2[0]) - 1
print("Size of hidden layer is", hidden_layer_size)

# 3
x2_sig = sigmoid(2)
print("Sigmoid result for x=2 is", x2_sig)
pred = h0x_func(X, [theta1, theta2])

# 4
pred = np.argmax(pred, axis=1) + 1
predictions = 0
for i in range(len(pred)):
    if pred[i] == y[i][0]:
        predictions += 1
acc = predictions / len(y) * 100
print("Prediction accuracy is", acc)

# 5
y_one_hot_trans = pd.get_dummies(y.squeeze())
labels_size = len(y_one_hot_trans.values[0])

# 6
lambda0_ = 0
loss_res = loss_func(neunet_params, hidden_layer_size, labels_size, X, y_one_hot_trans, lambda0_)
print("Loss Function result is", loss_res)

# 7
lambda1_ = 1
loss_res = loss_func(neunet_params, hidden_layer_size, labels_size, X, y_one_hot_trans, lambda1_)
print("Loss Function result is", loss_res)

# 8
sig_der = sigmoid_der(2)
print("Sigmoid derivative result is", sig_der)

# 9
input_layer_size = len(X[0])
num_labels = len(y_one_hot_trans.values[0])
initial_Theta1 = get_random_weights(input_layer_size, hidden_layer_size)
initial_Theta2 = get_random_weights(hidden_layer_size, num_labels)
initial_neunet_params = np.append(initial_Theta1.flatten(), initial_Theta2.flatten())

# 10
backprop_params = back_prop(initial_neunet_params, hidden_layer_size, labels_size, X, y_one_hot_trans, lambda0_)

# 11
e_check = 0.0001
gradient_check(initial_neunet_params, backprop_params, hidden_layer_size, X, y_one_hot_trans, lambda0_, e_check)

# 12,13
print("L2 regularization (lambda = 1):")
backprop_params = back_prop(initial_neunet_params, hidden_layer_size, labels_size, X, y_one_hot_trans, lambda1_)
gradient_check(initial_neunet_params, backprop_params, hidden_layer_size, X, y_one_hot_trans, lambda1_, e_check)

# 14
epochs = 15
theta_opt = opt.fmin_cg(f=loss_func, x0=initial_neunet_params, fprime=back_prop, args=(hidden_layer_size, labels_size, X, y_one_hot_trans, lambda1_), maxiter=epochs)
theta1_opt = np.reshape(theta_opt[:hidden_layer_size * (input_layer_size + 1)], (hidden_layer_size, input_layer_size + 1), 'F')
theta2_opt = np.reshape(theta_opt[hidden_layer_size * (input_layer_size + 1):], (num_labels, hidden_layer_size + 1), 'F')

# 15
pred2 = h0x_func(X, [theta1_opt, theta2_opt])
pred2 = np.argmax(pred2, axis=1) + 1
predictions2 = 0
for i in range(len(pred2)):
    if pred2[i] == y[i][0]:
        predictions2 += 1
acc2 = predictions2 / len(y) * 100
print("Prediction accuracy is", acc2)

# 16
display_figures(theta1_opt[:, 1:])

# 17
lambda_values = np.linspace(0, 0.5, 10)
validation_errors = []
accuracy = []
for lamb in lambda_values:
    theta_opt_lamb_i = opt.fmin_cg(f=loss_func, x0=initial_neunet_params, fprime=back_prop, args=(hidden_layer_size, labels_size, X, y_one_hot_trans, lamb), maxiter=epochs)
    validation_errors.append(loss_func(theta_opt_lamb_i, hidden_layer_size, labels_size, X, y_one_hot_trans, lambda0_))
    theta1_opt = np.reshape(theta_opt_lamb_i[:hidden_layer_size * (input_layer_size + 1)], (hidden_layer_size, input_layer_size + 1), 'F')
    theta2_opt = np.reshape(theta_opt_lamb_i[hidden_layer_size * (input_layer_size + 1):], (num_labels, hidden_layer_size + 1), 'F')
    pred3 = h0x_func(X, [theta1_opt, theta2_opt])
    pred3 = np.argmax(pred3, axis=1) + 1
    predictions3 = 0
    for i in range(len(pred3)):
        if pred[i] == y[i][0]:
            predictions3 += 1
    acc3 = predictions3 / len(y) * 100
    accuracy.append(acc3)
plt.plot(lambda_values, validation_errors)
plt.title("Validation Curve")
plt.xlabel("Lambda")
plt.ylabel("Error")
plt.grid()
plt.show()

plt.plot(lambda_values, accuracy)
plt.title("Validation Curve")
plt.xlabel("Lambda")
plt.ylabel("Accuracy")
plt.grid()
plt.show()

lambda2_ = 0.1
lambda3_ = 100
theta_opt_lam2 = opt.fmin_cg(f=loss_func, x0=initial_neunet_params, fprime=back_prop, args=(hidden_layer_size, labels_size, X, y_one_hot_trans, lambda2_), maxiter=epochs)
theta_opt_lam2 = np.reshape(theta_opt_lam2[:hidden_layer_size * (input_layer_size + 1)], (hidden_layer_size, input_layer_size + 1), 'F')
display_figures(theta_opt_lam2[:, 1:])

theta_opt_lam3 = opt.fmin_cg(f=loss_func, x0=initial_neunet_params, fprime=back_prop, args=(hidden_layer_size, labels_size, X, y_one_hot_trans, lambda3_), maxiter=epochs)
theta_opt_lam3 = np.reshape(theta_opt_lam3[:hidden_layer_size * (input_layer_size + 1)], (hidden_layer_size, input_layer_size + 1), 'F')
display_figures(theta_opt_lam3[:, 1:])
